import 'dart:convert';
import 'dart:async';

import 'package:rxdart/rxdart.dart';
import 'package:flutter_app/src/bloc/bloc.dart';
import 'package:flutter_app/src/model/profile.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;
import 'package:bloc/bloc.dart';

class ItemBloc extends Bloc<ItemEvent, ItemState> {
  final http.Client httpClient;

  ItemBloc({@required this.httpClient}) : super(ItemInitial());

  @override
  Stream<Transition<ItemEvent, ItemState>> transformEvents(
      Stream<ItemEvent> events,
      TransitionFunction<ItemEvent, ItemState> transitionFn,
      ) {
    return super.transformEvents(
      events.debounce((_) => TimerStream(true, Duration(milliseconds: 500))),
      transitionFn,
    );
  }

  @override
  Stream<ItemState> mapEventToState(ItemEvent event) async* {
    final currentState = state;
    if (event is ItemFetched && !_hasReachedMax(currentState)) {
      try {
        if (currentState is ItemInitial) {
          final posts = await _fetchPosts(0, 6);
          yield ItemSuccess(posts: posts, hasReachedMax: false);
          return;
        }
        if (currentState is ItemSuccess) {
          final posts = await _fetchPosts(currentState.posts.length, 6);
          yield posts.isEmpty
              ? currentState.copyWith(hasReachedMax: true)
              : ItemSuccess(
            posts: currentState.posts + posts,
            hasReachedMax: false,
          );
        }
      } catch (_) {
        yield ItemFailure();
      }
    }
  }

  bool _hasReachedMax(ItemState state) =>
      state is ItemSuccess && state.hasReachedMax;

  Future<List<Profile>> _fetchPosts(int startIndex, int limit) async {
    final response = await httpClient.get(
        'http://192.168.2.248:1111/api/shop?_start=$startIndex&_limit=$limit');
    if (response.statusCode == 200) {
      final data = json.decode(response.body)['dataShop'] as List;
      return data.map((rawPost) {
        return Profile(
          id: rawPost['id'],
          title: rawPost['title'],
          price: rawPost['price'],
          status: rawPost['status'],
          image: rawPost['image'],
        );
      }).toList();
    } else {
      throw Exception('error fetching posts');
    }
  }
}