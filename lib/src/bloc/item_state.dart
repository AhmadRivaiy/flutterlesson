import 'package:equatable/equatable.dart';
import 'package:flutter_app/src/model/profile.dart';

abstract class ItemState extends Equatable {
  const ItemState();

  @override
  List<Object> get props => [];
}

class ItemInitial extends ItemState {}

class ItemFailure extends ItemState {}

class ItemSuccess extends ItemState {
  final List<Profile> posts;
  final bool hasReachedMax;

  const ItemSuccess({
    this.posts,
    this.hasReachedMax,
  });

  ItemSuccess copyWith({
    List<Profile> posts,
    bool hasReachedMax,
  }) {
    return ItemSuccess(
      posts: posts ?? this.posts,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
    );
  }

  @override
  List<Object> get props => [posts, hasReachedMax];

  @override
  String toString() =>
      'ItemSuccess { posts: ${posts.length}, hasReachedMax: $hasReachedMax }';
}