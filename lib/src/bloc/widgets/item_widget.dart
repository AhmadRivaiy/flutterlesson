import 'package:flutter/material.dart';
import 'package:flutter_app/src/model/profile.dart';
import 'package:flutter_app/src/ui/detail/detail.dart';

class ItemWidget extends StatelessWidget {
  final Profile item;

  const ItemWidget({Key key, @required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // return ListTile(
    //   leading: Text(
    //     '${item.id}',
    //     style: TextStyle(fontSize: 10.0),
    //   ),
    //   title: Text(item.title),
    //   isThreeLine: true,
    //   subtitle: Text('sads'),
    //   dense: true,
    // );
    return Padding(
        padding: const EdgeInsets.only(top: 1.0),
        child:  GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => DetailScreen(id:item.id, title:item.title)),
            );
          },
          child: Card(
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget> [
                  Stack(
                      alignment: Alignment.bottomRight,
                      children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(6.0),
                              topRight: Radius.circular(6.0)
                          ),
                          child: Image.network(
                              item.image,
                              fit: BoxFit.fill
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.all(5.0),
                          color: Colors.redAccent,
                          child: Text(
                              item.status
                          ),
                        )
                      ]
                  ),
                  Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget> [
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0, top: 5.0),
                            child: Text(
                              item.title,
                              softWrap: true,
                              textAlign: TextAlign.start,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0, top: 5.0),
                            child: Text(
                              item.price,
                              textAlign: TextAlign.start,
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.only(left: 8.0, top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Icon(
                                  Icons.car_rental,
                                  color: Colors.pink,
                                  size: 18.0,
                                  semanticLabel: 'Text to announce in accessibility modes',
                                ),
                                Text(
                                  'Kota Bandung',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontWeight: FontWeight.w300),
                                ),
                              ],
                            ),
                          )
                        ],
                      )
                  )
                ],
              ),
            ),
          ),
        )
    );
  }
}