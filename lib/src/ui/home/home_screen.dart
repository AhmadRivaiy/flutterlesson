import 'package:flutter/material.dart';
import 'package:flutter_app/src/api/api_service.dart';
import 'package:flutter_app/src/bloc/bloc.dart';
import 'package:flutter_app/src/bloc/widgets/bottom_loader.dart';
import 'package:flutter_app/src/bloc/widgets/item_widget.dart';
import 'package:flutter_app/src/model/profile.dart';
import 'package:flutter_app/src/ui/detail/detail.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  ApiService apiService;
  final _scrollController = ScrollController();
  final _scrollThreshold = 200.0;
  ItemBloc _postBloc;

  @override
  void initState() {
    super.initState();
    apiService = ApiService();
    _scrollController.addListener(_onScroll);
    _postBloc = BlocProvider.of<ItemBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ItemBloc, ItemState>(
      builder: (context, state) {
        if (state is ItemFailure) {
          return Center(
            child: Text('failed to fetch posts'),
          );
        }else if (state is ItemSuccess) {
          if (state.posts.isEmpty) {
            return Center(
              child: Text('no posts'),
            );
          }
          return  Container(
            padding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 8.0),
            child: GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    crossAxisSpacing: 2,
                    mainAxisSpacing: 8,
                    height: 400
                ),
                itemBuilder: (BuildContext context, int index) {
                  return index >= state.posts.length
                      ? BottomLoader()
                      : ItemWidget(item: state.posts[index]);
                },
                itemCount: state.hasReachedMax
                    ? state.posts.length
                    : state.posts.length + 1,
                controller: _scrollController,
            )
          );
        } else {
          return const Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll <= _scrollThreshold) {
      _postBloc.add(ItemFetched());
    }
  }
}