import 'package:flutter/material.dart';

class Todo {
  final String id;
  final String title;
  Todo(this.id, this.title);
}

class DetailScreen extends StatelessWidget {

  final int id;
  final String title;

  const DetailScreen({Key key, this.id, this.title}) : super(key: key);
  // In the constructor, require a Todo.
  //DetailScreen({Key key, @required this.todo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text('Go Back Cuk : ' + id.toString()),
        ),
      ),
    );
  }
}