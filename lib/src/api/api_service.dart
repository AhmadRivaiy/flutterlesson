import 'package:flutter_app/src/model/profile.dart';
import 'package:http/http.dart' show Client;

class ApiService {

  final String baseUrl = "http://192.168.2.248:1111";
  Client client = Client();

  Future<List<Profile>> getProfiles() async {
    final response = await client.get("$baseUrl/api/shop");
    if (response.statusCode == 200) {
      return profileFromJson(response.body);
    } else {
      return null;
    }
  }

}