import 'dart:convert';

class Profile {
  int id;
  String title;
  String status;
  String price;
  String image;

  Profile({this.id = 0, this.title, this.status, this.price, this.image});

  factory Profile.fromJson(Map<String, dynamic> map) {
    return Profile(
        id: map["id"], title: map["title"], status: map["status"], price: map["price"], image: map["image"]);
  }

  Map<String, dynamic> toJson() {
    return {"id": id, "title": title, "status": status, "price": price, "image":image};
  }

  @override
  String toString() {
    return 'Profile{id: $id, title: $title, status: $status}';
  }

}

List<Profile> profileFromJson(String jsonData) {
  final data = json.decode(jsonData)['dataShop'];
  return List<Profile>.from(data.map((item) => Profile.fromJson(item)));
}

String profileToJson(Profile data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}